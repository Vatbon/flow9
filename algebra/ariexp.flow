import maybe;
import string;
import ds/tree;
import lingo/pegcode/driver;
import rational;

export{
    AriExp ::= SumExp, MulExp, SubExp, DivExp, MinExp, IntExp, VarExp;

    SumExp(l: AriExp, r: AriExp);
    SubExp(l: AriExp, r: AriExp);
    MulExp(l: AriExp, r: AriExp);
    DivExp(l: AriExp, r: AriExp);
    MinExp(expr: AriExp);
    IntExp(val: int);
    VarExp(name: string);

    ari2s(e: AriExp) -> string;
    s2ari(s: string) -> AriExp;
    evaluate(expr: AriExp, vars: Tree<string, int>) -> Maybe<Rational>;
    differentiate(expr: AriExp, var: string) -> AriExp;
    simplify(expr: AriExp) -> AriExp;
}

gram = "#include alg.gram"

ari2s(e: AriExp) -> string{
    switch (e) {
        SumExp(l,r): concatStrings(["(",ari2s(l),"+",ari2s(r),")"]);
        MulExp(l,r): concatStrings(["(",ari2s(l),"*",ari2s(r),")"]);
        SubExp(l,r): concatStrings(["(",ari2s(l),"-",ari2s(r),")"]);
        DivExp(l,r): concatStrings(["(",ari2s(l),"/",ari2s(r),")"]);
        MinExp(ex): concatStrings(["(-",ari2s(ex),")"]);
        IntExp(v): i2s(v);
        VarExp(var): var;
    }
}

s2ari(s: string) -> AriExp{
    parser=compilePegGrammar(gram);
    parsic(parser,s, defaultPegActions);
}

evaluate(expr: AriExp, vars: Tree<string, int>) -> Maybe<Rational>{
    switch(expr){
        SumExp(l,r): 
            switch(evaluate(l, vars)){
                Some(ll): switch(evaluate(r, vars)){
                    Some(rr): Some(sumRational(ll, rr));
                    None(): None();
                };
                None(): None();
            };
        MulExp(l,r): 
            switch(evaluate(l,vars)){
                Some(ll): switch(evaluate(r, vars)){
                    Some(rr): Some(mulRational(ll, rr));
                    None(): None();
                };
                None(): None();
            };
        SubExp(l,r): 
            switch(evaluate(l, vars)){
                Some(ll): switch(evaluate(r, vars)){
                    Some(rr): Some(subRational(ll, rr));
                    None(): None();
                };
                None(): None();
            };
        DivExp(l,r): 
            switch(evaluate(l, vars)){
                Some(ll): switch(evaluate(r, vars)){
                    Some(rr): if(rr.numenator == 0) None() 
                        else Some(divRational(ll, rr));
                    None(): None();
                };
                None(): None();
            };
        MinExp(e): evaluate(SubExp(IntExp(0), e), vars);
        IntExp(v): Some(Rational(v, 1));
        VarExp(name): 
            switch(lookupTree(vars, name)){
                Some(v): Some(Rational(v, 1));
                None(): None();
            };
    }
}

differentiate(expr: AriExp, var: string) -> AriExp{
    switch(expr){
        SumExp(l,r): SumExp(differentiate(l, var), differentiate(r, var));
        SubExp(l,r): SubExp(differentiate(l, var), differentiate(r, var));
        MulExp(l,r): SumExp(MulExp(differentiate(l,var), r), MulExp(l, differentiate(r, var)));
        DivExp(l,r): DivExp(SubExp(MulExp(differentiate(l,var), r), MulExp(l, differentiate(r, var))), MulExp(r,r));
        MinExp(e): MinExp(differentiate(e, var));
        IntExp(v): IntExp(0);
        VarExp(name): if (name == var) {IntExp(1)} else {IntExp(0)}; 
    }
}

isSimplifyable(expr: AriExp) -> bool{
    switch(expr){
        SumExp(l,r): if(l == IntExp(0) || r == IntExp(0)) {true} 
                     else {isSimplifyable(l) || isSimplifyable(r)};
        SubExp(l,r): if(l == IntExp(0) || r == IntExp(0)) {true} 
                     else {isSimplifyable(l) || isSimplifyable(r)};
        MulExp(l,r): if( l == IntExp(0) || r == IntExp(0)) {true} 
                     else if(l == IntExp(1) || r == IntExp(1)) {true}
                     else {isSimplifyable(l) || isSimplifyable(r)};
        DivExp(l,r): if ( l == r ) {true}
                     else if(r == IntExp(1)) {true} 
                     else {isSimplifyable(l) || isSimplifyable(r)};
        MinExp(e): isSimplifyable(e);
        IntExp(v): false;
        VarExp(name): false; 
    }
}

simplify(expr: AriExp) -> AriExp{
    if (isSimplifyable(expr)){
        switch(expr){
            SumExp(l,r): if(l == IntExp(0) && r == IntExp(0)) IntExp(0)
                        else if(l == IntExp(0)) simplify(r)
                        else if (r == IntExp(0)) simplify(l)
                        else simplify(SumExp(simplify(l), simplify(r)));
            SubExp(l,r): if(l == IntExp(0) && r == IntExp(0)) IntExp(0)
                        else if( l == r ) IntExp(0)
                        else if(l == IntExp(0)) simplify(r)
                        else if (r == IntExp(0)) simplify(l)
                        else simplify(SubExp(simplify(l), simplify(r)));
            MulExp(l,r): if( l == IntExp(0) || r == IntExp(0)) IntExp(0) 
                        else if(l == IntExp(1)) simplify(r)
                        else if (r == IntExp(1)) simplify(l)
                        else simplify(MulExp(simplify(l), simplify(r)));
            DivExp(l,r): if ( l == r ) IntExp(1)
                        else if(r == IntExp(1)) simplify(l)
                        else simplify(DivExp(simplify(l), simplify(r)));
            MinExp(expression): MinExp(simplify(expression));
            IntExp(v): IntExp(v);
            VarExp(name): VarExp(name); 
        }
    } else expr;
}